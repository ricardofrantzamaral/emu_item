package br.com.monolit.emusysadmin.utils.emu_item.table_list

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.Parametros
import br.com.monolit.emusysadmin.R
import org.jetbrains.anko.custom.style
import submodules.common.requests.objs.ContaSimple
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class ContaAdapter(list: ArrayList<ContaSimple?>, context: Context) : SimpleTableAdapter<ContaSimple, ContaTableListViewHolder, ContaTableListViewHolder> (list, context) {
    override fun getHeaderLayout() = R.layout.item_conta

    override fun returnHeaderViewHolder(view: View) = ContaTableListViewHolder(view)

    override fun bindListHeader(holder: ContaTableListViewHolder, position: Int) {
        holder.desc.text = "Descrição"
        holder.date.text = "Data"
        holder.price.text = "Valor"
    }

    override fun getItemlayout(): Int = R.layout.item_conta

    override fun returnViewHolder(view: View) = ContaTableListViewHolder(view)

    override fun bindData(holder: ContaTableListViewHolder, position: Int) {
        val dateFormat = SimpleDateFormat("dd/MM", Locale.ENGLISH)
        val numberFormat = NumberFormat.getCurrencyInstance(Parametros.LOCALE)
        val decimalFormatSymbols = (numberFormat as DecimalFormat).decimalFormatSymbols
        decimalFormatSymbols.currencySymbol = ""
        (numberFormat as DecimalFormat).decimalFormatSymbols = decimalFormatSymbols
        val date = Date()
        date.time = list[position]!!.time_stamp_venc

        holder.desc.text = list[position]?.name
        holder.date.text = dateFormat.format(date)
        holder.price.text = numberFormat.format(list[position]?.value).trim()
    }
}