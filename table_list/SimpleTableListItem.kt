package br.com.monolit.emusysadmin.utils.emu_item.table_list

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import br.com.monolit.emusysadmin.utils.emu_item.pie_chart.SimplePieChartViewHolder

class SimpleTableListItem<A : Any, B : RecyclerView.ViewHolder, C : RecyclerView.ViewHolder> (val context: Context,
                                                                          val title: String,
                                                                          val adapter: SimpleTableAdapter<A, B, C>,
                                                                          @ColorRes val color: Int? = null ) : EmuItem<TableListRecyclerViewHolder>(){

    override fun createViewHolder(parent: ViewGroup, context: Context): TableListRecyclerViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_table_list_recycler, parent, false)

        return TableListRecyclerViewHolder(view)
    }

    override fun customizeView(viewHolder: TableListRecyclerViewHolder) {
        viewHolder.title.text = title

        var layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        viewHolder.recyclerView.layoutManager = layoutManager
        viewHolder.recyclerView.adapter = adapter
        viewHolder.recyclerView.isNestedScrollingEnabled = false
    }
}