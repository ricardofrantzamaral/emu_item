package br.com.monolit.emusysadmin.utils.emu_item.table_list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import br.com.monolit.emusysadmin.R
import kotlinx.android.synthetic.main.item_conta.view.*

class ContaTableListViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
    val layout : ViewGroup
    val desc : TextView
    val price : TextView
    val date : TextView

    init {
        layout = itemView.findViewById(R.id.item_conta)
        desc = itemView.findViewById(R.id.tv_desc)
        price = itemView.findViewById(R.id.tv_price)
        date = itemView.findViewById(R.id.tv_date)
    }

}