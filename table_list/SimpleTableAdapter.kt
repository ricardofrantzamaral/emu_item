package br.com.monolit.emusysadmin.utils.emu_item.table_list

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem

abstract class SimpleTableAdapter <T : Any, A : RecyclerView.ViewHolder, H : RecyclerView.ViewHolder>
(var list : ArrayList<T?>, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    init {
        list.add(0, null)
    }
    abstract fun getHeaderLayout() : Int
    abstract fun returnHeaderViewHolder(view: View) : RecyclerView.ViewHolder
    abstract fun bindListHeader(holder: H, position: Int)

    abstract fun getItemlayout() : Int
    abstract fun returnViewHolder(view: View) : RecyclerView.ViewHolder
    abstract fun bindData(holder: A, position: Int)

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)

        if (viewType == 0) {
            val view = inflater.inflate(getHeaderLayout(), parent, false)

            return returnHeaderViewHolder(view)
        } else {
            val view = inflater.inflate(getItemlayout(), parent, false)

            return returnViewHolder(view)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        if (position == 0){
            bindListHeader(holder as H, position)
        } else {
            bindData(holder as A, position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (list[position] == null) {
            0
        }
        else {
            1
        }
    }
}