package br.com.monolit.emusysadmin.utils.emu_item.pagination_table.students

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.table_list.ContaTableListViewHolder
import br.com.monolit.emusysadmin.utils.emu_item.table_list.SimpleTableAdapter
import submodules.common.requests.objs.ContaSimple
import submodules.common.requests.objs.StudentObject
import java.util.ArrayList

class StudentTableAdapter (list: ArrayList<StudentObject?>, context: Context) : SimpleTableAdapter<StudentObject, StudentViewHolder, StudentViewHolder>(list, context) {
    override fun bindListHeader(holder: StudentViewHolder, position: Int) {
        holder.name.text = "Nomes"
        holder.courses.text = "Cursos"
        holder.conclusionDates.text = "Datas de Conclusão"
    }

    override fun bindData(holder: StudentViewHolder, position: Int) {
        holder.name.text = list[position]?.nome
        holder.courses.text = list[position]?.cursoNomes
        holder.conclusionDates.text = list[position]?.finalCursos
    }

    override fun getHeaderLayout(): Int {
        return R.layout.item_student
    }

    override fun returnHeaderViewHolder(view: View): RecyclerView.ViewHolder {
        return StudentViewHolder(view)
    }

    override fun getItemlayout(): Int {
        return R.layout.item_student
    }

    override fun returnViewHolder(view: View): RecyclerView.ViewHolder {
        return StudentViewHolder(view)
    }

    fun addToList(newList: ArrayList<StudentObject?>) {
        list.addAll(newList)
        notifyDataSetChanged()
    }

    fun renewList(newList: ArrayList<StudentObject?>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }
}