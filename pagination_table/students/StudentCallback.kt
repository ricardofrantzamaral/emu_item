package br.com.monolit.emusysadmin.utils.emu_item.pagination_table.students

import android.content.Context
import android.support.v7.widget.RecyclerView
import br.com.monolit.emusysadmin.App
import br.com.monolit.emusysadmin.server.APIListener
import br.com.monolit.emusysadmin.server.into
import br.com.monolit.emusysadmin.server.retrofit.API
import br.com.monolit.emusysadmin.submodules.common.requests.conn.StudentsRequest
import br.com.monolit.emusysadmin.utils.emu_item.pagination_table.SimplePaginateCallback
import br.com.monolit.emusysadmin.utils.emu_item.table_list.SimpleTableAdapter
import submodules.common.requests.objs.StudentObject

class StudentCallback(val context: Context, adapter: StudentTableAdapter, recyclerView: RecyclerView) : SimplePaginateCallback<StudentsRequest.PageResponse, StudentObject>(recyclerView = recyclerView,
        adapter = adapter as SimpleTableAdapter<StudentObject, RecyclerView.ViewHolder, RecyclerView.ViewHolder>) {
    override fun loadMore(listener: APIListener<StudentsRequest.PageResponse>) {
        API.connect().students_page(App.getApiBearerToken(context), StudentsRequest.PageParameter(page)).into(context, false, listener)
    }

    override fun checkSuccess(result: StudentsRequest.PageResponse) {
        if (result.students.isEmpty()) {
            loading = false
            finished = true
            return
        }

        (adapter as StudentTableAdapter).addToList(ArrayList(result.students))
        loading = false
    }
}