package br.com.monolit.emusysadmin.utils.emu_item.pagination_table.students

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import br.com.monolit.emusysadmin.R

class StudentViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
    val layout : ViewGroup
    val name : TextView
    val courses : TextView
    val conclusionDates : TextView

    init {
        layout = itemView.findViewById(R.id.item_student)
        name = itemView.findViewById(R.id.tv_student_name)
        courses = itemView.findViewById(R.id.tv_student_course)
        conclusionDates = itemView.findViewById(R.id.tv_student_conclusion)
    }

}