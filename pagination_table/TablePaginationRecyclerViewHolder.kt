package br.com.monolit.emusysadmin.utils.emu_item.pagination_table

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import br.com.monolit.emusysadmin.R

class TablePaginationRecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val layout : LinearLayout
    val title : TextView
    val recyclerView: RecyclerView

    init {
        layout = itemView.findViewById(R.id.item_paginated_table_list_recycler)
        title = itemView.findViewById(R.id.tv_title)
        recyclerView = itemView.findViewById(R.id.main_recycler)
    }
}