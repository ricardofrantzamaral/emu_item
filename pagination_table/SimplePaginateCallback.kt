package br.com.monolit.emusysadmin.utils.emu_item.pagination_table

import android.support.v7.widget.RecyclerView
import br.com.monolit.emusysadmin.server.APIListener
import br.com.monolit.emusysadmin.submodules.common.requests.conn.APIResponse
import br.com.monolit.emusysadmin.utils.emu_item.table_list.SimpleTableAdapter
import com.paginate.Paginate
import retrofit2.Call

abstract class SimplePaginateCallback<T : APIResponse, A : Any>  (val recyclerView: RecyclerView,
                                                                  val adapter: SimpleTableAdapter<A, RecyclerView.ViewHolder, RecyclerView.ViewHolder>): Paginate.Callbacks {

    var loading = false
    var finished = false
    var page = 0

    var paginate : Paginate? = null

    abstract fun loadMore(listener: APIListener<T>)
    abstract fun checkSuccess(result: T)

    fun setPaginateInstance(paginate: Paginate){
        this.paginate = paginate
    }

    override fun onLoadMore() {
        loading = true
        loadMore(object : APIListener<T>() {
            override fun onSuccess(result: T) {
                checkSuccess(result)
                if (!finished){
                    page++
                } else {
                    val recyclerViewState = recyclerView.layoutManager.onSaveInstanceState()
                    paginate?.unbind()
                    recyclerView.layoutManager.onRestoreInstanceState(recyclerViewState)
                }
            }

            override fun onFailure(call: Call<T>?, t: Throwable?) {
                loading = false
            }

        })
    }

    override fun isLoading(): Boolean {
        return loading
    }

    override fun hasLoadedAllItems(): Boolean {
        return finished
    }
}