package br.com.monolit.emusysadmin.utils.emu_item.pagination_table

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.submodules.common.requests.conn.APIResponse
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import br.com.monolit.emusysadmin.utils.emu_item.table_list.SimpleTableAdapter
import com.paginate.Paginate

class SimplePaginationListItem<A : Any, B : RecyclerView.ViewHolder, H : RecyclerView.ViewHolder, C : APIResponse>(val context: Context,
                                                                                                                   val title: String,
                                                                                                                   val adapter: SimpleTableAdapter<A, B, H>,
                                                                                                                   val callbacks : SimplePaginateCallback<C, A>,
                                                                                                                   @ColorRes val color: Int? = null) : EmuItem<TablePaginationRecyclerViewHolder>() {
    override fun createViewHolder(parent: ViewGroup, context: Context): TablePaginationRecyclerViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.item_paginated_table_list_recycler, parent, false)

        return TablePaginationRecyclerViewHolder(view)
    }

    override fun customizeView(viewHolder: TablePaginationRecyclerViewHolder) {
        viewHolder.title.text = title

        var layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        viewHolder.recyclerView.layoutManager = layoutManager
        viewHolder.recyclerView.adapter = adapter
        viewHolder.recyclerView.isNestedScrollingEnabled = false

        val build = Paginate.with(viewHolder.recyclerView, callbacks)
                .setLoadingTriggerThreshold(2)
                .build()
        callbacks.setPaginateInstance(build)
    }
}