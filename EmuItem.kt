package br.com.monolit.emusysadmin.utils.emu_item

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R

/**
 * Created by Ricardo on 14/03/2018.
 */
abstract class EmuItem<T : RecyclerView.ViewHolder>() {

    companion object {
        fun getColorsArray(context: Context): IntArray {
            return intArrayOf(ContextCompat.getColor(context, R.color.blue_600),
                    ContextCompat.getColor(context, R.color.red_800),
                    ContextCompat.getColor(context, R.color.green_800),
                    ContextCompat.getColor(context, R.color.purple_800),
                    ContextCompat.getColor(context, R.color.cyan_800),
                    ContextCompat.getColor(context, R.color.orange_800),
                    ContextCompat.getColor(context, R.color.teal_800),
                    ContextCompat.getColor(context, R.color.pink_800),
                    ContextCompat.getColor(context, R.color.light_blue_800),
                    ContextCompat.getColor(context, R.color.amber_800))
        }
    }

    abstract fun createViewHolder(parent: ViewGroup, context: Context): T

    fun bind(viewHolder: RecyclerView.ViewHolder) {
        @Suppress("UNCHECKED_CAST")
        customizeView(viewHolder as T)
    }

    protected abstract fun customizeView(viewHolder: T)
}