package br.com.monolit.emusysadmin.utils.emu_item

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

/**
 * Created by Ricardo on 14/03/2018.
 */
class EmuAdapter(var list : ArrayList<EmuItem<*>>, val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return list[viewType].createViewHolder(parent, context)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        list[position].bind(holder)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


}