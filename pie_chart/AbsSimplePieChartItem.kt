package br.com.monolit.emusysadmin.utils.emu_item.pie_chart

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import br.com.monolit.emusysadmin.utils.emu_item.bar_chart.SimpleBarChartViewHolder
import br.com.monolit.emusysadmin.utils.emu_item.formatter.NumberValueFormatter
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IValueFormatter

abstract class AbsSimplePieChartItem<T : Any> (val context: Context,
                                               val title: String,
                                               val message : String? = null,
                                               val dataList: List<T>,
                                               val dataValueFormatter: IValueFormatter? = null,
                                               @ColorRes val backgroundColor: Int? = null,
                                               @ColorRes val pieColors: List<Int>? = null) : EmuItem<SimplePieChartViewHolder>(){

    abstract fun dataToPieEntry(index: Int, data: T): PieEntry
    abstract fun configDataSet(pieEntryList: List<PieEntry>): PieDataSet
    abstract fun configPieData(pieDataSet: PieDataSet): PieData
    abstract fun configChart(chart: PieChart)
    open fun configViewHolder(viewHolder: SimplePieChartViewHolder){}

    override fun createViewHolder(parent: ViewGroup, context: Context): SimplePieChartViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.simple_piechart_item, parent, false)

        return SimplePieChartViewHolder(view)
    }

    override fun customizeView(viewHolder: SimplePieChartViewHolder) {

        if (backgroundColor != null) {
            viewHolder.layout.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.context, backgroundColor))
        }

        viewHolder.button.visibility = View.GONE
        viewHolder.tvTitle.text = title

        if (message.isNullOrEmpty()) {
            viewHolder.tvMessage.visibility = View.GONE
        } else {
            viewHolder.tvMessage.text = message
        }

        configViewHolder(viewHolder)

        val pieEntries = dataList.mapIndexed { index, t -> dataToPieEntry(index, t) }
        val pieDataSet = configDataSet(pieEntries)

        if (pieColors != null){
            pieDataSet.colors = pieColors.map { ContextCompat.getColor(context, it) }
        } else {
            val colorsArray = getColorsArray(context)
            pieDataSet.colors = colorsArray.toList()
        }

        val pieData = configPieData(pieDataSet)

        if (dataValueFormatter != null) {
            pieData.setValueFormatter(dataValueFormatter)
        }

        viewHolder.piechart.data = pieData

        configChart(viewHolder.piechart)
    }
}