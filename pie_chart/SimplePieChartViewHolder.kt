package br.com.monolit.emusysadmin.utils.emu_item.pie_chart

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import br.com.monolit.emusysadmin.R
import com.github.mikephil.charting.charts.PieChart

class SimplePieChartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val layout : LinearLayout
    val tvTitle : TextView
    val tvMessage : TextView
    val tvDesc: TextView
    val button: Button
    val piechart : PieChart

    init {
        layout = itemView.findViewById(R.id.simple_piechart_item)
        tvTitle = itemView.findViewById(R.id.piechart_title)
        tvMessage = itemView.findViewById(R.id.piechart_message)
        tvDesc = itemView.findViewById(R.id.piechart_desc)
        button = itemView.findViewById(R.id.chart_button)
        piechart = itemView.findViewById(R.id.simple_piechart)
    }
}