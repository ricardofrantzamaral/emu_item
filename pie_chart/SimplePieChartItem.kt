package br.com.monolit.emusysadmin.utils.emu_item.pie_chart

import android.content.Context
import android.graphics.Color
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.NumberUtils
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import submodules.common.requests.conn.ChartEntry

class SimplePieChartItem (context: Context,
                          title: String,
                          dataList: List<ChartEntry>,
                          @ColorRes pieColors: List<Int>? = null)
    : AbsSimplePieChartItem<ChartEntry>(context, title, dataList = dataList, pieColors = pieColors) {

    override fun dataToPieEntry(index: Int, data: ChartEntry): PieEntry {
        return PieEntry(data.value, data.label)
    }

    override fun configDataSet(pieEntryList: List<PieEntry>): PieDataSet {
        val pieDataSet = PieDataSet(pieEntryList, "")
        pieDataSet.yValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE
        pieDataSet.setValueTextColors(listOf(ContextCompat.getColor(context, R.color.white)))
        pieDataSet.valueTextSize = 10f
        return pieDataSet
    }

    override fun configPieData(pieDataSet: PieDataSet): PieData {
        val pieData = PieData(pieDataSet)
        pieData.setDrawValues(false)
        return pieData
    }

    override fun configChart(chart: PieChart) {
        chart.offsetLeftAndRight(0)
        chart.setExtraOffsets(0f,0f,0f,0f)
        chart.circleBox.offset(0f,0f)

        chart.isRotationEnabled = false
        chart.isDrawHoleEnabled = true
        chart.holeRadius = 70f
        chart.setHoleColor(ContextCompat.getColor(context, R.color.foreground_emusys))
        chart.description.isEnabled = false
        chart.setDrawEntryLabels(false)
        chart.setDrawCenterText(true)
        chart.setCenterTextColor(Color.WHITE)
        chart.centerText = NumberUtils().getCurrencyNoSymbol(chart.data.yValueSum)
        chart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener{
            override fun onNothingSelected() {
                chart.centerText = NumberUtils().getCurrencyNoSymbol(chart.data.yValueSum)
            }

            override fun onValueSelected(e: Entry?, h: Highlight?) {
                if (e != null) {
                    chart.centerText = NumberUtils().getCurrencyNoSymbol((e as PieEntry).value)
                }
            }
        })

        val l = chart.legend
        l.isEnabled = true
        l.verticalAlignment = Legend.LegendVerticalAlignment.CENTER
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)
        l.textColor = ContextCompat.getColor(chart.context, R.color.white)

        chart.invalidate()
    }
}