package br.com.monolit.emusysadmin.utils.emu_item.formatter

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter

class PercentAxisFormatter () : IAxisValueFormatter {
    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        return String.format("%.0f%%", value)
    }
}