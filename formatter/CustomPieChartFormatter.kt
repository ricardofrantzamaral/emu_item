package br.com.monolit.emusysadmin.utils.emu_item.formatter

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler

class CustomPieChartFormatter : IValueFormatter{
    override fun getFormattedValue(value: Float, entry: Entry?, dataSetIndex: Int, viewPortHandler: ViewPortHandler?): String {

        return String.format("%.0f - %s",value, (entry as PieEntry).label)
    }
}