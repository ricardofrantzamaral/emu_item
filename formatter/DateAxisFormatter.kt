package br.com.monolit.emusysadmin.utils.emu_item.formatter

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DateAxisFormatter(val mDataFormat: DateFormat = SimpleDateFormat("dd/MM", Locale.ENGLISH),
                        private var mDate: Date = Date()) : IAxisValueFormatter {
    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        // convertedTimestamp = originalTimestamp - referenceTimestamp
        val convertedTimestamp = value.toLong()

        return getDate(convertedTimestamp)
    }

    private fun getDate(timestamp: Long): String {
        try {
            mDate.setTime(timestamp)
            return mDataFormat.format(mDate)
        } catch (ex: Exception) {
            return "xx"
        }

    }

}