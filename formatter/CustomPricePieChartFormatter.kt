package br.com.monolit.emusysadmin.utils.emu_item.formatter

import br.com.monolit.emusysadmin.Parametros
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class CustomPricePieChartFormatter : IValueFormatter {
    override fun getFormattedValue(value: Float, entry: Entry?, dataSetIndex: Int, viewPortHandler: ViewPortHandler?): String {

        val numberFormat = NumberFormat.getCurrencyInstance(Parametros.LOCALE)
        val decimalFormatSymbols = (numberFormat as DecimalFormat).decimalFormatSymbols
        decimalFormatSymbols.currencySymbol = ""
        (numberFormat as DecimalFormat).decimalFormatSymbols = decimalFormatSymbols
        return numberFormat.format(value).trim()
    }
}