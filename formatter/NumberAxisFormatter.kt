package br.com.monolit.emusysadmin.utils.emu_item.formatter

import br.com.monolit.emusysadmin.Parametros
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import java.text.DecimalFormat
import java.text.NumberFormat

class NumberAxisFormatter(val nDecimals : Int = 0) : IAxisValueFormatter {
    override fun getFormattedValue(value: Float, axis: AxisBase?): String {

        val numberFormat : NumberFormat
        if (nDecimals == 0) {
            numberFormat = NumberFormat.getIntegerInstance(Parametros.LOCALE)
        } else {
            numberFormat = NumberFormat.getInstance(Parametros.LOCALE)
            numberFormat.minimumFractionDigits = nDecimals
        }

        return numberFormat.format(value).trim()
    }
}