package br.com.monolit.emusysadmin.utils.emu_item.formatter

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class StringAxisFormatter (val labelsList : List<String>) : IAxisValueFormatter {
    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
        val convertedValue = value.toInt()

        return labelsList[convertedValue % labelsList.size]
    }
}