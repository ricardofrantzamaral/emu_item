package br.com.monolit.emusysadmin.utils.emu_item.formatter

import br.com.monolit.emusysadmin.Parametros
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler
import java.text.NumberFormat

class NumberValueFormatter(val nDecimals : Int = 0) : IValueFormatter{
    override fun getFormattedValue(value: Float, entry: Entry?, dataSetIndex: Int, viewPortHandler: ViewPortHandler?): String {
        val numberFormat : NumberFormat
        if (nDecimals == 0) {
            numberFormat = NumberFormat.getIntegerInstance(Parametros.LOCALE)
        } else {
            numberFormat = NumberFormat.getInstance(Parametros.LOCALE)
            numberFormat.minimumFractionDigits = nDecimals
        }

        return numberFormat.format(value).trim()
    }
}