package br.com.monolit.emusysadmin.utils.emu_item.bar_chart.conta

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.View
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.bar_chart.AbsSimpleBarChartItem
import br.com.monolit.emusysadmin.utils.emu_item.bar_chart.SimpleBarChartViewHolder
import br.com.monolit.emusysadmin.utils.emu_item.formatter.NumberAxisFormatter
import br.com.monolit.emusysadmin.utils.emu_item.formatter.NumberValueFormatter
import br.com.monolit.emusysadmin.utils.emu_item.formatter.StringAxisFormatter
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener

class ContaBarChartItem(context: Context, title: String, dataList : List<BarEntry>,
                        val labelList: List<String>)
    : AbsSimpleBarChartItem<BarEntry>(context = context, title = title, dataList = dataList,
        xAxisFormatter = StringAxisFormatter(labelList),
         yAxisFormatter = NumberAxisFormatter()){
    override fun dataToBarEntry(index: Int, data: BarEntry): BarEntry {
        return data
    }

    override fun configDataSet(barEntryList: List<BarEntry>): BarDataSet {
        val barDataSet = BarDataSet(barEntryList, "")
        barDataSet.valueTextColor = ContextCompat.getColor(context, R.color.white)
        barDataSet.valueFormatter = NumberValueFormatter()
        return barDataSet
    }

    override fun configBarData(barDataSet: BarDataSet): BarData {
        return BarData(barDataSet)
    }

    override fun configChart(chart: BarChart) {
        chart.legend.isEnabled = false
        chart.isScaleYEnabled = false
        chart.viewPortHandler.setMaximumScaleX(8f)
        chart.setDrawBarShadow(false)
        chart.setDrawGridBackground(false)
        chart.description.isEnabled = false
        chart.setFitBars(true)
        chart.xAxis.granularity = 1f
        chart.xAxis.setDrawGridLines(false)
        chart.xAxis.textColor = ContextCompat.getColor(context, R.color.white)
        chart.axisLeft.granularity = 1f
        chart.axisLeft.textColor = ContextCompat.getColor(context, R.color.white)
        chart.axisLeft.gridColor = ContextCompat.getColor(context, R.color.grey_900)
        chart.axisRight.isEnabled = false

        chart.invalidate()
    }

    override fun configViewHolder(viewHolder: SimpleBarChartViewHolder) {
        viewHolder.tvDesc.visibility = View.INVISIBLE
        viewHolder.barChart.setOnChartValueSelectedListener(object: OnChartValueSelectedListener {
            override fun onNothingSelected() {
            }
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                viewHolder.tvDesc.visibility = View.VISIBLE
                if (e?.data != null) {
                    viewHolder.tvDesc.text = e.data as String
                }
            }

        })
    }
}