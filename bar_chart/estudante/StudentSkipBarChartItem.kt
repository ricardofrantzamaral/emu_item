package br.com.monolit.emusysadmin.utils.emu_item.bar_chart.estudante

import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.View
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.bar_chart.AbsSimpleBarChartItem
import br.com.monolit.emusysadmin.utils.emu_item.bar_chart.SimpleBarChartViewHolder
import br.com.monolit.emusysadmin.utils.emu_item.formatter.NumberValueFormatter
import br.com.monolit.emusysadmin.utils.emu_item.formatter.StringAxisFormatter
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import submodules.common.requests.objs.StudentSkipping

class StudentSkipBarChartItem(context: Context, title: String, dataList : List<StudentSkipping>)
    : AbsSimpleBarChartItem<StudentSkipping>(context = context, title = title, dataList = dataList){
    override fun dataToBarEntry(index: Int, data: StudentSkipping): BarEntry {
        return BarEntry(index.toFloat(), data.p_faltas, data.nome)
    }

    override fun configDataSet(barEntryList: List<BarEntry>): BarDataSet {
        val barDataSet = BarDataSet(barEntryList, "")
        barDataSet.valueTextColor = ContextCompat.getColor(context, R.color.white)
        barDataSet.setDrawValues(false)
        return barDataSet
    }

    override fun configBarData(barDataSet: BarDataSet): BarData {
        return BarData(barDataSet)
    }

    override fun configChart(chart: BarChart) {
        chart.legend.isEnabled = false
        chart.viewPortHandler.setMaximumScaleX(8f)
        chart.viewPortHandler.setMaximumScaleY(8f)
        chart.setDrawBarShadow(false)
        chart.setDrawGridBackground(false)
        chart.description.isEnabled = false
        chart.setFitBars(true)
        chart.xAxis.granularity = 1f
        chart.xAxis.setDrawGridLines(false)
        chart.xAxis.textColor = ContextCompat.getColor(context, R.color.white)
        chart.axisLeft.granularity = 0.1f
        chart.axisLeft.axisMinimum = 0f
        chart.axisLeft.textColor = ContextCompat.getColor(context, R.color.white)
        chart.axisLeft.gridColor = ContextCompat.getColor(context, R.color.grey_900)
        chart.axisLeft.valueFormatter = PercentFormatter()
        chart.axisRight.isEnabled = false

        chart.invalidate()
    }

    override fun configViewHolder(viewHolder: SimpleBarChartViewHolder) {
        viewHolder.tvDesc.visibility = View.INVISIBLE
        viewHolder.barChart.setOnChartValueSelectedListener(object: OnChartValueSelectedListener {
            override fun onNothingSelected() {
            }
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                viewHolder.tvDesc.visibility = View.VISIBLE
                if (e?.data != null) {
                    viewHolder.tvDesc.text = e.data as String
                }
            }
        })
    }

}