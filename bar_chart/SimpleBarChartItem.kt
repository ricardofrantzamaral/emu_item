package br.com.monolit.emusysadmin.utils.emu_item.bar_chart

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener

class SimpleBarChartItem(val title: String,
                         val message: String? = null,
                         val context: Context,
                         val values: List<BarEntry>,
                         val legend: List<String>,
                         @ColorRes val color: Int? = null,
                         val barChartColor: Int? = null,
                         val listener: View.OnClickListener? = null) : EmuItem<SimpleBarChartViewHolder>() {
    override fun createViewHolder(parent: ViewGroup, context: Context): SimpleBarChartViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.simple_bar_chart_item, parent, false)

        return SimpleBarChartViewHolder(view)
    }

    override fun customizeView(viewHolder: SimpleBarChartViewHolder) {
        viewHolder.button.visibility = View.GONE
        viewHolder.tvTitle.text = title

        if (message.isNullOrEmpty()) {
            viewHolder.tvMessage.visibility = View.GONE
        } else {
            viewHolder.tvMessage.text = message
        }

        val dataSet = BarDataSet(values, "")
        if (barChartColor != null) {
            dataSet.color = barChartColor
        } else {
            dataSet.color = getColorsArray(context)[0]
        }

        val data = BarData(dataSet)

        viewHolder.barChart.setOnChartValueSelectedListener(object: OnChartValueSelectedListener{
            override fun onNothingSelected() {
            }
            override fun onValueSelected(e: Entry?, h: Highlight?) {
                viewHolder.tvDesc.visibility = View.VISIBLE
                viewHolder.tvDesc.text = e?.data as String
            }

        })

        configChart(viewHolder.barChart, data)

        if (color != null) {
            viewHolder.layout.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.context, color))
        }

        if (listener != null) {
            viewHolder.layout.setOnClickListener(listener)
        }

    }

    private fun configChart(chart: BarChart, data: BarData) {
        chart.data = data

        chart.legend.isEnabled = false
        chart.setPinchZoom(false)
        chart.setScaleEnabled(false)
        chart.setDrawBarShadow(false)
        chart.setDrawGridBackground(false)
        chart.description.isEnabled = false
        chart.setFitBars(true)
        chart.xAxis.isEnabled = false

        chart.invalidate()
    }
}