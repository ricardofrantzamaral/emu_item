package br.com.monolit.emusysadmin.utils.emu_item.bar_chart

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import br.com.monolit.emusysadmin.R
import com.github.mikephil.charting.charts.BarChart

class SimpleBarChartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val layout: LinearLayout
    val tvTitle: TextView
    val tvMessage: TextView
    val tvDesc: TextView
    val button: Button
    val barChart: BarChart

    init {
        layout = itemView.findViewById(R.id.simple_bar_chart_item)
        tvTitle = itemView.findViewById(R.id.bar_chart_title)
        tvMessage = itemView.findViewById(R.id.bar_chart_message)
        tvDesc = itemView.findViewById(R.id.bar_chart_desc)
        button = itemView.findViewById(R.id.chart_button)
        barChart = itemView.findViewById(R.id.simple_barchart)
    }
}