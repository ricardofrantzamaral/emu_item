package br.com.monolit.emusysadmin.utils.emu_item.bar_chart

import android.content.Context
import android.service.autofill.Dataset
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import br.com.monolit.emusysadmin.utils.emu_item.formatter.PercentAxisFormatter
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.listener.OnChartValueSelectedListener

abstract class AbsSimpleBarChartItem<T : Any>(val context: Context,
                                              val title: String,
                                              val message: String? = null,
                                              val dataList: List<T>,
                                              @ColorRes val barColor: Int = R.color.blue_600,
                                              @ColorRes val backgroundColor: Int? = null,
                                              val xAxisFormatter: IAxisValueFormatter? = null,
                                              val yAxisFormatter: IAxisValueFormatter? = null) : EmuItem<SimpleBarChartViewHolder>() {

    abstract fun dataToBarEntry(index: Int, data: T): BarEntry
    abstract fun configDataSet(barEntryList: List<BarEntry>): BarDataSet
    abstract fun configBarData(barDataSet: BarDataSet): BarData
    abstract fun configChart(chart: BarChart)
    open fun configViewHolder(viewHolder: SimpleBarChartViewHolder){}

    override fun createViewHolder(parent: ViewGroup, context: Context): SimpleBarChartViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.simple_bar_chart_item, parent, false)

        return SimpleBarChartViewHolder(view)
    }

    override fun customizeView(viewHolder: SimpleBarChartViewHolder) {

        if (backgroundColor != null) {
            viewHolder.layout.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.context, backgroundColor))
        }

        viewHolder.button.visibility = View.GONE
        viewHolder.tvTitle.text = title

        if (message.isNullOrEmpty()) {
            viewHolder.tvMessage.visibility = View.GONE
        } else {
            viewHolder.tvMessage.text = message
        }

        configViewHolder(viewHolder)

        val barEntryList = dataList.mapIndexed { index, t -> dataToBarEntry(index, t) }

        val barDataSet = configDataSet(barEntryList)
        barDataSet.color = ContextCompat.getColor(viewHolder.itemView.context, barColor)

        val data = configBarData(barDataSet)

        viewHolder.barChart.data = data

        if (xAxisFormatter != null) {
            viewHolder.barChart.xAxis.valueFormatter = xAxisFormatter
        }

        if (yAxisFormatter != null) {
            viewHolder.barChart.axisLeft.valueFormatter = yAxisFormatter
            viewHolder.barChart.axisRight.valueFormatter = yAxisFormatter
        }

        configChart(viewHolder.barChart)
    }
}