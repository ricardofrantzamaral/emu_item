package br.com.monolit.emusysadmin.utils.emu_item.line_chart

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet

abstract class AbsMultiLineChartItem<T : Any>(val title: String,
                            val message: String? = null,
                            val context: Context,
                            val dataLists: List<List<T>>,
                            @ColorRes val backgroundColor: Int? = null,
                            @ColorRes val lineColors: List<Int>? = null,
                            val xAxisFormatter: IAxisValueFormatter? = null,
                            val yAxisFormatter: IAxisValueFormatter? = null): EmuItem<SimpleLineChartViewHolder>() {

    abstract fun dataToEntry(index: Int, data: T): Entry
    abstract fun configLineDataSet(entryLists: List<Entry>): LineDataSet
    abstract fun configLineData(lineDataSets: List<LineDataSet>): LineData
    abstract fun configChart(chart: LineChart)
    open fun configViewHolder(viewHolder: SimpleLineChartViewHolder){}

    override fun createViewHolder(parent: ViewGroup, context: Context): SimpleLineChartViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.simple_line_chart_item, parent, false)

        return SimpleLineChartViewHolder(view)
    }

    override fun customizeView(viewHolder: SimpleLineChartViewHolder) {
        if (backgroundColor != null) {
            viewHolder.layout.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.context, backgroundColor))
        }

        viewHolder.button.visibility = View.GONE
        viewHolder.tvTitle.text = title

        if (message.isNullOrEmpty()) {
            viewHolder.tvMessage.visibility = View.GONE
        } else {
            viewHolder.tvMessage.text = message
        }

        configViewHolder(viewHolder)

        val entryLists = dataLists.map { it.mapIndexed { index, t -> dataToEntry(index, t) } }
        val dataSets = entryLists.map { configLineDataSet(it) }
        val lengendEntries = dataSets.map { LegendEntry(it.label,
                it.form, it.formSize, it.formLineWidth, it.formLineDashEffect, it.color) }

        val dotDateSets = ArrayList<LineDataSet>()

        if (lineColors != null) {
            for ((i, dataSet) in dataSets.withIndex()) {
                dataSet.color = ContextCompat.getColor(context, lineColors[i % lineColors.size])
                val ddot = LineDataSet(listOf(dataSet.values[dataSet.values.size-1]), "")
                ddot.circleRadius = 5f
                ddot.isHighlightEnabled = false
                ddot.color = ContextCompat.getColor(context, lineColors[i % lineColors.size])
                ddot.setCircleColor(ContextCompat.getColor(context, lineColors[i % lineColors.size]))
                ddot.setCircleColorHole(ContextCompat.getColor(context, lineColors[i % lineColors.size]))
                dotDateSets.add(ddot)
            }
        }

        dotDateSets.addAll(dataSets)
        val lineData = configLineData(dotDateSets)

        viewHolder.lineChart.data = lineData
        viewHolder.lineChart.legend.setCustom(lengendEntries)

        if (xAxisFormatter != null) {
            viewHolder.lineChart.xAxis.valueFormatter = xAxisFormatter
        }

        if (yAxisFormatter != null) {
            viewHolder.lineChart.axisLeft.valueFormatter = yAxisFormatter
            viewHolder.lineChart.axisRight.valueFormatter = yAxisFormatter
        }

        configChart(viewHolder.lineChart)
    }
}