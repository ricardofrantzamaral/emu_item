package br.com.monolit.emusysadmin.utils.emu_item.line_chart

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import br.com.monolit.emusysadmin.utils.emu_item.formatter.StringAxisFormatter
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import java.util.*
import kotlin.collections.ArrayList

class MultiLineChartItem(val title: String,
                         val message: String? = null,
                         val context: Context,
                         vararg valuesLists: List<Entry>,
                         val legendList: List<String>,
                         val xAxisLabels: List<String>,
                         @ColorRes val color: Int? = null,
                         val lineChartColors: IntArray? = null,
                         val listener: View.OnClickListener? = null) : EmuItem<SimpleLineChartViewHolder>() {

    var dataSets = ArrayList<ILineDataSet>()
    var legendEntries = ArrayList<LegendEntry>()
    val axisFormatter : StringAxisFormatter

    init {
        for ((i, valueList) in valuesLists.withIndex()) {
            val d: LineDataSet

            if (!legendList[i].isNullOrEmpty()) {
                d = LineDataSet(valueList, legendList[i])
            } else {
                d = LineDataSet(valueList, "")
            }

            d.setDrawCircles(false)
            d.lineWidth = 2.5f

            if (lineChartColors != null) {
                d.color = lineChartColors[i % lineChartColors.size]
            } else {
                val colorsArray = getColorsArray(context)
                d.color = colorsArray[i % colorsArray.size]
            }

            d.setDrawFilled(true)
            d.mode = LineDataSet.Mode.CUBIC_BEZIER
            d.setValueTextColors(listOf(ContextCompat.getColor(context, R.color.white)))
            d.fillAlpha = 10
            d.fillColor = ContextCompat.getColor(context, R.color.white)
            d.isHighlightEnabled = false
            d.setDrawVerticalHighlightIndicator(false)
            d.setDrawHorizontalHighlightIndicator(false)

            val ddot = LineDataSet(listOf(valueList[valueList.size-1]), "")
            ddot.circleRadius = 5f
            ddot.isHighlightEnabled = false

            if (lineChartColors != null) {
                ddot.color = lineChartColors[i % lineChartColors.size]
                ddot.setCircleColor(lineChartColors[i % lineChartColors.size])
                ddot.setCircleColorHole(lineChartColors[i % lineChartColors.size])
            } else {
                val colorsArray = getColorsArray(context)
                ddot.color = colorsArray[i % colorsArray.size]
                ddot.setCircleColor(colorsArray[i % colorsArray.size])
                ddot.setCircleColorHole(colorsArray[i % colorsArray.size])
            }

            dataSets.add(d)
            legendEntries.add(LegendEntry(d.label, d.form, d.formSize, d.formLineWidth, d.formLineDashEffect, d.color))
            dataSets.add(ddot)
        }

        axisFormatter = StringAxisFormatter(xAxisLabels)
    }

    override fun createViewHolder(parent: ViewGroup, context: Context): SimpleLineChartViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.simple_line_chart_item, parent, false)

        return SimpleLineChartViewHolder(view)
    }

    override fun customizeView(viewHolder: SimpleLineChartViewHolder) {
        viewHolder.button.visibility = View.GONE
        viewHolder.tvTitle.text = title

        if (message.isNullOrEmpty()){
            viewHolder.tvMessage.visibility = View.GONE
        } else{
            viewHolder.tvMessage.text = message
        }

        val lineData = LineData(dataSets)
        //lineData.setValueFormatter(LargeValueFormatter())
        lineData.setDrawValues(false)

        configChart(viewHolder.lineChart, lineData)

        viewHolder.lineChart.setOnChartValueSelectedListener(object: OnChartValueSelectedListener {
            override fun onNothingSelected() {
                viewHolder.tvDesc.visibility = View.GONE
            }

            override fun onValueSelected(e: Entry?, h: Highlight?) {

            }
        })

        if (color != null) {
            viewHolder.layout.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.context, color))
        }
        if (listener != null) {
            viewHolder.layout.setOnClickListener(listener)
        }
    }

    private fun configChart(chart: LineChart, lineData: LineData) {
        chart.data = lineData
        chart.setDrawGridBackground(false)
        chart.description.isEnabled = false

        chart.legend.setCustom(legendEntries)

        chart.axisRight.isEnabled = false
        chart.axisLeft.isGranularityEnabled = true
        chart.axisLeft.granularity = 1f
        chart.axisLeft.textColor = ContextCompat.getColor(chart.context, R.color.white)
        chart.axisLeft.gridColor = ContextCompat.getColor(chart.context, R.color.grey_900)

        chart.xAxis.valueFormatter = axisFormatter

        chart.viewPortHandler.setMaximumScaleY(8f)
        chart.viewPortHandler.setMaximumScaleX(8f)

        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.xAxis.setDrawGridLines(true)
        chart.xAxis.gridColor = ContextCompat.getColor(chart.context, R.color.grey_900)
        chart.xAxis.setCenterAxisLabels(false)
        chart.xAxis.granularity = 1f
        chart.xAxis.labelCount = 4
        chart.xAxis.textColor = ContextCompat.getColor(chart.context, R.color.white)

        chart.legend.textColor = ContextCompat.getColor(chart.context, R.color.white)

        chart.invalidate()
    }
}