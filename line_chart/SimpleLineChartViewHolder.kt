package br.com.monolit.emusysadmin.utils.emu_item.line_chart

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import br.com.monolit.emusysadmin.R
import com.github.mikephil.charting.charts.LineChart

class SimpleLineChartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
    val layout : LinearLayout
    val tvTitle : TextView
    val tvMessage : TextView
    val tvDesc: TextView
    val button: Button
    val lineChart : LineChart

    init {
        layout = itemView.findViewById(R.id.simple_line_chart_item)
        tvTitle = itemView.findViewById(R.id.line_chart_title)
        tvMessage = itemView.findViewById(R.id.line_chart_message)
        tvDesc = itemView.findViewById(R.id.line_chart_desc)
        button = itemView.findViewById(R.id.chart_button)
        lineChart = itemView.findViewById(R.id.simple_linechart)
    }
}