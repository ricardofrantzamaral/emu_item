package br.com.monolit.emusysadmin.utils.emu_item.line_chart

import android.content.Context
import android.graphics.Color
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import br.com.monolit.emusysadmin.utils.emu_item.bar_chart.SimpleBarChartViewHolder
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IAxisValueFormatter

abstract class AbsSimpleLineChartItem <T : Any>(val context: Context,
                                                val title: String,
                                                val message : String? = null,
                                                val dataList: List<T>,
                                                @ColorRes val backgroundColor: Int? = null,
                                                @ColorRes val lineColor: Int? = null,
                                                val xAxisFormatter: IAxisValueFormatter? = null,
                                                val yAxisFormatter: IAxisValueFormatter? = null) : EmuItem<SimpleLineChartViewHolder>() {

    abstract fun dataToEntry(index: Int, data: T): Entry
    abstract fun configLineDataSet(EntryList: List<Entry>): LineDataSet
    abstract fun configLineData(lineDataSet: LineDataSet): LineData
    abstract fun configChart(chart: LineChart)
    open fun configViewHolder(viewHolder: SimpleLineChartViewHolder){}


    override fun createViewHolder(parent: ViewGroup, context: Context): SimpleLineChartViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.simple_line_chart_item, parent, false)

        return SimpleLineChartViewHolder(view)
    }

    override fun customizeView(viewHolder: SimpleLineChartViewHolder) {
        if (backgroundColor != null) {
            viewHolder.layout.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.context, backgroundColor))
        }

        viewHolder.button.visibility = View.GONE
        viewHolder.tvTitle.text = title

        if (message.isNullOrEmpty()) {
            viewHolder.tvMessage.visibility = View.GONE
        } else {
            viewHolder.tvMessage.text = message
        }

        configViewHolder(viewHolder)

        val lineEntryList = dataList.mapIndexed {index, t ->  dataToEntry(index, t) }

        val lineDataSet = configLineDataSet(lineEntryList)

        val dataSetDot = LineDataSet(listOf(lineEntryList[lineEntryList.size-1]), "")
        dataSetDot.circleRadius = 5f
        dataSetDot.isHighlightEnabled = false
        val legendEntry = LegendEntry(dataSetDot.label,
                dataSetDot.form,
                dataSetDot.formSize,
                dataSetDot.formLineWidth,
                dataSetDot.formLineDashEffect,
                dataSetDot.color)

        if (lineColor != null) {
            lineDataSet.color = ContextCompat.getColor(context, lineColor)
            dataSetDot.color = ContextCompat.getColor(context, lineColor)
            dataSetDot.setCircleColor(ContextCompat.getColor(context, lineColor))
            dataSetDot.setCircleColorHole(ContextCompat.getColor(context, lineColor))
        }


        val data = configLineData(lineDataSet)
        data.addDataSet(dataSetDot)

        viewHolder.lineChart.data = data
        viewHolder.lineChart.legend.setCustom(listOf(legendEntry))

        if (xAxisFormatter != null) {
            viewHolder.lineChart.xAxis.valueFormatter = xAxisFormatter
        }

        if (yAxisFormatter != null) {
            viewHolder.lineChart.axisLeft.valueFormatter = yAxisFormatter
            viewHolder.lineChart.axisRight.valueFormatter = yAxisFormatter
        }

        configChart(viewHolder.lineChart)
    }
}