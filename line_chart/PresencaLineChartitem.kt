package br.com.monolit.emusysadmin.utils.emu_item.line_chart

import android.content.Context
import android.support.v4.content.ContextCompat
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.formatter.StringAxisFormatter
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import submodules.common.requests.objs.DateValueEntry

class PresencaLineChartitem (context: Context, title: String, dataList : List<DateValueEntry>,
                             val labelList: List<String>): AbsSimpleLineChartItem<DateValueEntry>(context, title, dataList = dataList,
        xAxisFormatter = StringAxisFormatter(labelList)){
    override fun dataToEntry(index: Int, data: DateValueEntry): Entry {
        return Entry(index.toFloat(), data.value, data.time_stamp)
    }

    override fun configLineDataSet(EntryList: List<Entry>): LineDataSet {
        val lineDataSet = LineDataSet(EntryList, "Presenças")
        lineDataSet.fillAlpha = 10
        lineDataSet.fillColor = ContextCompat.getColor(context, R.color.white)
        lineDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        lineDataSet.setDrawCircles(false)
        lineDataSet.setValueTextColors(listOf(ContextCompat.getColor(context, R.color.white)))
        return lineDataSet
    }

    override fun configLineData(lineDataSet: LineDataSet): LineData {
        val lineData = LineData(lineDataSet)
        lineData.setDrawValues(false)
        return lineData
    }

    override fun configChart(chart: LineChart) {
        chart.setDrawGridBackground(false)
        chart.description.isEnabled = false
        chart.legend.isEnabled = false

        chart.axisRight.isEnabled = false
        chart.axisLeft.axisMinimum = 0f
        chart.axisLeft.isGranularityEnabled = true
        chart.axisLeft.granularity = 1f
        chart.axisLeft.textColor = ContextCompat.getColor(chart.context, R.color.white)
        chart.axisLeft.gridColor = ContextCompat.getColor(chart.context, R.color.grey_900)

        chart.isScaleYEnabled = false
        chart.viewPortHandler.setMaximumScaleX(8f)

        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.xAxis.setDrawGridLines(true)
        chart.xAxis.setCenterAxisLabels(false)
        chart.xAxis.granularity = 1f
        chart.xAxis.labelCount = 4
        chart.xAxis.textColor = ContextCompat.getColor(chart.context, R.color.white)
        chart.xAxis.gridColor = ContextCompat.getColor(chart.context, R.color.grey_900)

        chart.isHighlightPerDragEnabled = false
        chart.isHighlightPerTapEnabled = false

        chart.invalidate()
    }
}