package br.com.monolit.emusysadmin.utils.emu_item.line_chart

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import br.com.monolit.emusysadmin.utils.emu_item.formatter.StringAxisFormatter
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import java.util.ArrayList

class LineChartItem(val title: String,
                    val message : String? = null,
                    val setName: String,
                    val values: List<Entry>,
                    val xAxisLabels: List<String>,
                    @ColorRes val color: Int? = null,
                    val lineChartColors: IntArray? = null,
                    val listener: View.OnClickListener? = null) : EmuItem<SimpleLineChartViewHolder>() {

    var dataSets = ArrayList<ILineDataSet>()

    override fun createViewHolder(parent: ViewGroup, context: Context): SimpleLineChartViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.simple_line_chart_item, parent, false)

        return SimpleLineChartViewHolder(view)
    }

    override fun customizeView(viewHolder: SimpleLineChartViewHolder) {
        viewHolder.button.visibility = View.GONE
        viewHolder.tvTitle.text = title

        if (message.isNullOrEmpty()){
            viewHolder.tvMessage.visibility = View.GONE
        } else{
            viewHolder.tvMessage.text = message
        }

        // Line DataSet
        val dataSet = LineDataSet(values, setName)
        dataSet.isHighlightEnabled = true

        if (lineChartColors != null && lineChartColors.isNotEmpty()) {
            dataSet.colors = lineChartColors.toList()
        } else {
            dataSet.color =  getColorsArray(viewHolder.lineChart.context)[0]
        }

        dataSet.setDrawFilled(true)
        dataSet.fillAlpha = 10
        dataSet.fillColor = ContextCompat.getColor(viewHolder.lineChart.context, R.color.white)
        dataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        dataSet.setDrawCircles(false)
        dataSet.setValueTextColors(listOf(ContextCompat.getColor(viewHolder.lineChart.context, R.color.white)))

        // Last Point Highlight
        val datasetDot = LineDataSet(listOf(values[values.size-1]), setName)
        datasetDot.circleRadius = 5f
        datasetDot.isHighlightEnabled = false

        if (lineChartColors != null && lineChartColors.isNotEmpty()) {
            datasetDot.color = lineChartColors[0]
            datasetDot.setCircleColor(lineChartColors[0])
            datasetDot.setCircleColorHole(lineChartColors[0])
        } else {
            datasetDot.color =  getColorsArray(viewHolder.lineChart.context)[0]
            datasetDot.setCircleColor(getColorsArray(viewHolder.lineChart.context)[0])
            datasetDot.setCircleColorHole(getColorsArray(viewHolder.lineChart.context)[0])
        }

        dataSets.add(dataSet)
        dataSets.add(datasetDot)

        val data = LineData(dataSets)
        data.setDrawValues(false)

        viewHolder.lineChart.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onNothingSelected() {

            }

            override fun onValueSelected(e: Entry?, h: Highlight?) {

            }

        })

        configChart(viewHolder.lineChart, data)

        if (color != null) {
            viewHolder.layout.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.context, color))
        }

        if (listener != null) {
            viewHolder.layout.setOnClickListener(listener)
        }
    }

    private fun configChart(chart: LineChart, data: LineData) {
        chart.data = data
        chart.setDrawGridBackground(false)
        chart.description.isEnabled = false
        chart.legend.isEnabled = false

        chart.axisRight.isEnabled = false
        chart.axisLeft.axisMinimum = 0f
        chart.axisLeft.isGranularityEnabled = true
        chart.axisLeft.granularity = 1f
        chart.axisLeft.textColor = ContextCompat.getColor(chart.context, R.color.white)
        chart.axisLeft.gridColor = ContextCompat.getColor(chart.context, R.color.grey_900)

        chart.isScaleYEnabled = false
        chart.viewPortHandler.setMaximumScaleX(8f)

        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.xAxis.valueFormatter = StringAxisFormatter(xAxisLabels)
        chart.xAxis.setDrawGridLines(true)
        chart.xAxis.setCenterAxisLabels(false)
        chart.xAxis.granularity = 1f
        chart.xAxis.labelCount = 4
        chart.xAxis.textColor = ContextCompat.getColor(chart.context, R.color.white)
        chart.xAxis.gridColor = ContextCompat.getColor(chart.context, R.color.grey_900)

        chart.isHighlightPerDragEnabled = false
        chart.isHighlightPerTapEnabled = false

        chart.invalidate()
    }
}