package br.com.monolit.emusysadmin.utils.emu_item.line_chart

import android.content.Context
import android.support.annotation.ColorRes
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import br.com.monolit.emusysadmin.utils.emu_item.formatter.PercentAxisFormatter
import br.com.monolit.emusysadmin.utils.emu_item.formatter.StringAxisFormatter
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet

class PercentDottedLineChartItem(val title: String,
                                 val message : String? = null,
                                 val setName: String,
                                 val values: List<Entry>,
                                 val xAxisLabels: List<String>,
                                 @ColorRes val color: Int? = null,
                                 val lineChartColors: IntArray? = null,
                                 val listener: View.OnClickListener? = null) : EmuItem<SimpleLineChartViewHolder>() {

    override fun createViewHolder(parent: ViewGroup, context: Context): SimpleLineChartViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.simple_line_chart_item, parent, false)

        return SimpleLineChartViewHolder(view)
    }

    override fun customizeView(viewHolder: SimpleLineChartViewHolder) {
        viewHolder.button.visibility = View.GONE
        viewHolder.tvTitle.text = title

        if (message.isNullOrEmpty()){
            viewHolder.tvMessage.visibility = View.GONE
        } else{
            viewHolder.tvMessage.text = message
        }

        val dataSet = LineDataSet(values, setName)
        dataSet.isHighlightEnabled = false

        if (lineChartColors != null && lineChartColors.isNotEmpty()) {
            dataSet.color = lineChartColors[0]
            dataSet.setCircleColor(lineChartColors[0])
        } else {
            dataSet.color =  getColorsArray(viewHolder.lineChart.context)[0]
            dataSet.setCircleColor(getColorsArray(viewHolder.lineChart.context)[0])
        }


        dataSet.setDrawFilled(true)
        dataSet.fillAlpha = 10
        dataSet.fillColor = ContextCompat.getColor(viewHolder.lineChart.context, R.color.white)
        dataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        dataSet.setValueTextColors(listOf(ContextCompat.getColor(viewHolder.lineChart.context, R.color.white)))

        val data = LineData(dataSet)
        data.setDrawValues(false)

        configChart(viewHolder.lineChart, data)

        if (color != null) {
            viewHolder.layout.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.context, color))
        }

        if (listener != null) {
            viewHolder.layout.setOnClickListener(listener)
        }
    }

    private fun configChart(chart: LineChart, data: LineData) {
        chart.data = data
        chart.setDrawGridBackground(false)
        chart.description.isEnabled = false
        chart.legend.isEnabled = false

        chart.axisRight.isEnabled = false
        chart.axisLeft.axisMinimum = 0f
        chart.axisLeft.axisMaximum = 100f
        chart.axisLeft.isGranularityEnabled = true
        chart.axisLeft.granularity = 1f
        chart.axisLeft.textColor = ContextCompat.getColor(chart.context, R.color.white)
        chart.axisLeft.gridColor = ContextCompat.getColor(chart.context, R.color.grey_900)
        chart.axisLeft.valueFormatter = PercentAxisFormatter()

        chart.isScaleYEnabled = false
        chart.viewPortHandler.setMaximumScaleX(8f)

        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.xAxis.valueFormatter = StringAxisFormatter(xAxisLabels)
        chart.xAxis.setDrawGridLines(true)
        chart.xAxis.setCenterAxisLabels(false)
        chart.xAxis.granularity = 1f
        chart.xAxis.labelCount = 4
        chart.xAxis.textColor = ContextCompat.getColor(chart.context, R.color.white)
        chart.xAxis.gridColor = ContextCompat.getColor(chart.context, R.color.grey_900)

        chart.invalidate()
    }
}