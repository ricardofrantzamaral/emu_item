package br.com.monolit.emusysadmin.utils.emu_item.simple_item

import android.content.Context
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.monolit.emusysadmin.Parametros
import br.com.monolit.emusysadmin.R
import br.com.monolit.emusysadmin.utils.emu_item.EmuItem
import java.text.NumberFormat

class SimplePriceItem (val title: String,
                       val message: String,
                       val value: Float,
                       val numberFormat: NumberFormat = NumberFormat.getInstance(Parametros.LOCALE),
                       @DrawableRes val icon: Int? = null,
                       @ColorRes val color: Int? = null,
                       @ColorRes val text_color: Int? = null,
                       val listener: View.OnClickListener? = null) : EmuItem<SimpleViewHolder>() {
    override fun createViewHolder(parent: ViewGroup, context: Context): SimpleViewHolder {
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.simple_emu_item, parent, false)

        return SimpleViewHolder(view)
    }

    override fun customizeView(viewHolder: SimpleViewHolder) {
        viewHolder.tvTitle.text = title

        if (message.isNullOrEmpty()){
            viewHolder.tvMessage.visibility = View.GONE
        } else{
            viewHolder.tvMessage.text = message
        }

        viewHolder.tvValue.text = numberFormat.format(value)

        if (icon != null) {
            viewHolder.ivIcon.setImageDrawable(ContextCompat.getDrawable(viewHolder.itemView.context, icon))
        } else {
            viewHolder.ivIcon.visibility = View.GONE
        }

        if (color != null) {
            viewHolder.layout.setBackgroundColor(ContextCompat.getColor(viewHolder.itemView.context, color))
        }

        if (text_color != null) {
            viewHolder.tvMessage.setTextColor(ContextCompat.getColor(viewHolder.itemView.context, text_color))
            viewHolder.tvValue.setTextColor(ContextCompat.getColor(viewHolder.itemView.context, text_color))
        }

        if (listener != null) {
            viewHolder.layout.setOnClickListener(listener)
        }
    }
}
