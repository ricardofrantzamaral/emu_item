package br.com.monolit.emusysadmin.utils.emu_item.simple_item

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import br.com.monolit.emusysadmin.R

/**
 * Created by Ricardo on 14/03/2018.
 */

class SimpleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val layout : LinearLayout
    val tvTitle : TextView
    val tvMessage : TextView
    val tvValue : TextView
    val ivIcon : ImageView

    init {
        layout = itemView.findViewById(R.id.simple_emu_item)
        tvTitle = itemView.findViewById(R.id.simple_title)
        tvMessage = itemView.findViewById(R.id.simple_message)
        tvValue = itemView.findViewById(R.id.simple_text_value)
        ivIcon = itemView.findViewById(R.id.simple_icon)
    }

}